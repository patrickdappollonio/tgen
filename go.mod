module github.com/patrickdappollonio/tgen

require (
	github.com/inconshreveable/mousetrap v1.0.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.1
)
